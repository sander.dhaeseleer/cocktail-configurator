# Cocktail Configurator

## Project voorstelling
Dit  project bestaat uit het maken van een website waar cocktails besteld kunnen worden. Deze website kan gebruikt worden voor bijvoorbeeld een cocktail automaat of voor een hippe bar waar je via een QR code op de webpagina komt en er de cocktails en mocktails naar keuze kunt bestellen. 
Als er naar de website gesurft wordt kom je op de homepagina terecht. Rechts bovenaan is er de keuze om naar ‘AANBOD’, ‘NIEUWS’ of het winkelwagentje te gaan. Bij aanbod is er de keuze tussen standaard cocktails, standaard mocktails en ‘MAKE YOUR OWN’. Bij make your own kan de klant/gebruiker zelf cocktails samenstellen. Er zijn enkele ingrediënten en opties die gekozen kunnen worden zoals: welke dranken, hoeveel van elke drank en of er een rietje in moet. De cocktail die zelf samengesteld is kan in 3D weergegeven worden met A-frame. De weergave in A-frame wordt aangepast in functie van de gekozen opties, ook het kleur van de cocktail zal mee veranderen naar gelang de keuze van de dranken. Als de keuzes voor de cocktail gemaakt zijn, kan de cocktail toegevoegd worden aan een bestelling. Ook de standaard cocktails en mocktails kunnen aan de bestelling toegevoegd worden.  
Als de bestelling volledig is kan deze besteld worden. Elke bestelling wordt opgeslagen in een firebase databank. Per cocktail wordt er informatie in de databank bijgehouden zoals: wie deze besteld heeft, hoeveel er besteld zijn en wat de prijs is. 
Bij de keuze ‘NIEUWS’ rechts boven in de header kom je terecht op een pagina waar via RSS-feeds nieuwsberichten worden op gepost.
De website bevat ook animaties, zo komen de headers en titel in de pagina gevallen bij het inladen.



## Doelgroep
+18, studenten, feestgangers, mensen die van een alcoholische versnapering houden


## SMART-doelen voor dit project
-Cocktail 3D weergeven via A-frame
-Web animation maken voor tijdens het wachten op de cocktail
-RSS-feed sturen naar de gebruiker als de cocktail klaar is
-Data over de cocktails opslaan in een Firebase Realtime Database
-Data uit de database weergeven op een webpagina


## Behaalde SMART-doelen voor dit project
-DIY-cocktail 3D weergeven via A-frame
-Web animation maken door middel van GSAP
-RSS-feed nieuws berichten posten op de website via een extern tekst bestand
-Data over de cocktails opslaan in een Firebase Realtime Database
-Gebruik van functional-CSS over het volledige project
-Gebruik van JavaScript en HTML


## Extra informatie over de code
Elke web pagina heeft een achterliggende HTML- en Javascript pagina. De stijl van de website is volledig opgebouwd met tailwind-CSS.


## Toepassingen van de topics

### A-frame:
Voor het 3D animeren van de cocktail die wordt samengesteld door een klant/gebruiker.

### Web animations (CSS + JS, bv GSAP):
Er worden web animations gebruikt in de header van elke web pagina. Deze animations zijn mogelijk gemaakt met GSAP.

### RSS-feed:
RSS-feeds worden gebruikt om via een extern tekstbestand nieuws posts te plaatsen op de website.

### Firebase Realtime Database:
Opslaan van gegevens over de klant (welke cocktail besteld, hoeveel en de prijs)

### Functional CSS:
In het volledige project is tailwind gebruikt als functional-CSS.
