gsap.from(".header", {duration: 0.5, delay: 0, y: -100});
gsap.from(".text", {rotation: -360, x: -100, duration: 1});

var valueSliderTotal = 0;
var valueSliderVodka = 0;
var valueSliderRum = 0;
var valueSliderPassoa = 0;
var valueSliderJuice = 0;
var valueSliderSprite = 0;
var valueSliderCranberry = 0;
var valueSliderPeach = 0;
var valueSliderLemon = 0;

var roodTotal = 0;
var groenTotal = 0;
var blauwTotal = 0;

var kleurHex;

var boolPassoa = "false";
var boolPassoaLast = "false";
var boolJuice = "false";
var boolJuiceLast = "false";
var boolCranberry = "false";
var boolCranberryLast = "false";
var boolPeach = "false";
var boolPeachLast = "false";
var boolLemon = "false";
var boolLemonLast = "false";
var boolrietje = "false";

let valueDiytail = 0


document.getElementById("valueTotal").innerHTML = "%";

function functionVodka() {
    valueSliderVodka = document.getElementById("sliderVodka").value
    document.getElementById("valueVodka").innerHTML = valueSliderVodka
    checkTotal();
}

function functionRum() {
    valueSliderRum = document.getElementById("sliderRum").value
    document.getElementById("valueRum").innerHTML = valueSliderRum
    checkTotal();
}

function functionPassoa() {
    valueSliderPassoa = document.getElementById("sliderPassoa").value
    document.getElementById("valuePassoa").innerHTML = valueSliderPassoa
    checkTotal();
    if (valueSliderPassoa> 0) {
        boolPassoa = "true"
        checkBool();
    } else {
        boolPassoa = "false"
        checkBool();
    }
}

function functionJuice() {
    valueSliderJuice = document.getElementById("sliderJuice").value
    document.getElementById("valueJuice").innerHTML = valueSliderJuice
    checkTotal();
    if (valueSliderJuice> 0) {
        boolJuice = "true"
        checkBool();
    } else {
        boolJuice = "false"
        checkBool();
    }
}

function functionSprite() {
    valueSliderSprite = document.getElementById("sliderSprite").value
    document.getElementById("valueSprite").innerHTML = valueSliderSprite
    checkTotal();
}

function functionCranberry() {
    valueSliderCranberry = document.getElementById("sliderCranberry").value
    document.getElementById("valueCranberry").innerHTML = valueSliderCranberry
    checkTotal();
    if (valueSliderCranberry> 0) {
        boolCranberry = "true"
        checkBool();
    } else {
        boolCranberry = "false"
        checkBool();
    }
}

function functionPeach() {
    valueSliderPeach = document.getElementById("sliderPeach").value
    document.getElementById("valuePeach").innerHTML = valueSliderPeach
    checkTotal();
    if (valueSliderPeach> 0) {
        boolPeach = "true"
        checkBool();
    } else {
        boolPeach = "false"
        checkBool();
    }
}

function functionLemon() {
    valueSliderLemon = document.getElementById("sliderLemon").value
    document.getElementById("valueLemon").innerHTML = valueSliderLemon
    checkTotal();
    if (valueSliderLemon> 0) {
        boolLemon = "true"
        checkBool();
    } else {
        boolLemon = "false"
        checkBool();
    }
}

function checkTotal() {
    valueSliderTotal = parseInt(valueSliderVodka) + parseInt(valueSliderRum) + parseInt(valueSliderPassoa) + parseInt(valueSliderJuice) + parseInt(valueSliderSprite) + parseInt(valueSliderCranberry) + parseInt(valueSliderPeach) + parseInt(valueSliderLemon);
    document.getElementById("valueTotal").innerHTML = valueSliderTotal + "%";
    if (valueSliderTotal >= "101") {
        document.getElementById("valueTotal").style.color = "red";
    } else {
        document.getElementById("valueTotal").style.color = "black";
    }
}

function valueToHex(c) {
    var hex = c.toString(16);
    return hex
}

function changeColor(rood, groen, blauw) {
    roodTotal = parseInt(roodTotal) + parseInt(rood);
    roodTotal = parseInt(roodTotal) / 2;
    roodTotal = parseInt(roodTotal).toFixed();
    
    groenTotal = parseInt(groenTotal) + groen;
    groenTotal = parseInt(groenTotal) / 2;
    groenTotal = parseInt(groenTotal).toFixed();

    blauwTotal = parseInt(blauwTotal) + blauw;
    blauwTotal = parseInt(blauwTotal) / 2;
    blauwTotal = parseInt(blauwTotal).toFixed();
    
    kleurHex = "#"+(valueToHex(parseInt(roodTotal)) + valueToHex(parseInt(groenTotal)) + valueToHex(parseInt(blauwTotal)));
    document.getElementById("cylinderGlas1").setAttribute("color" , kleurHex);
}

function checkBool() {
    if(boolPassoa == "false" || boolJuice == "false" || boolCranberry == "false"  || boolPeach == "false" || boolLemon == "false") {
        roodTotal = 0;
        groenTotal = 0;
        blauwTotal = 0;
        document.getElementById("cylinderGlas1").setAttribute("color" , "#FFFFFF");
        if (boolPassoa == "true") {
            changeColor(244, 47, 47);
            boolPassoaLast = boolPassoa;
        }
        if (boolJuice == "true") {
            changeColor(244, 193, 47);
            boolJuiceLast = boolJuice;
        }
        if (boolCranberry == "true") {
            changeColor(116, 35, 35);
            boolCranberryLast = boolCranberry;
        }
        if (boolPeach == "true") {
            changeColor(227, 178, 35);
            boolPeachLast = boolPeach;
        }
        if (boolLemon== "true") {
            changeColor(218, 247, 166);
            boolLemonLast = boolLemon;
        }
    }
    
    if (boolPassoa != boolPassoaLast) {
        if (boolPassoa == "true") {
            changeColor(244, 47, 47);
            boolPassoaLast = boolPassoa;
        }
    }

    if (boolJuice != boolJuiceLast) {
        if (boolJuice == "true") {
            changeColor(244, 193, 47);
            boolJuiceLast = boolJuice;
        }
    }

    if (boolCranberry != boolCranberryLast) {
        if (boolCranberry == "true") {
            changeColor(116, 35, 35);
            boolCranberryLast = boolCranberry;
        }
    }
    
    if(boolPeach != boolPeachLast) {
        if (boolPeach == "true") {
            changeColor(227, 178, 35);
            boolPeachLast = boolPeach;
        }
    }

    if(boolLemon != boolLemonLast) {
        if (boolLemon== "true") {
            changeColor(218, 247, 166);
            boolLemonLast = boolLemon;
        }
    }
}

function rietje() {
    if(boolrietje == "false") {
        document.getElementById("rietjeGlas1_id").setAttribute('visible', 'true');
        boolrietje = "true";
    } else {
        document.getElementById("rietjeGlas1_id").setAttribute('visible', 'false');
        boolrietje = "false";
    }  
}

// af- en optelfunctie Diytail
function aftellenDiytail() {
    valueDiytail = parseInt(document.getElementById("Diytail").value)
    if(valueDiytail == 0){
        valueDiytail = 0;
    } else {
        valueDiytail = valueDiytail - 1;
    }
    document.getElementById("Diytail").setAttribute("value", valueDiytail);
}

function optellenDiytail() {
    valueDiytail = parseInt(document.getElementById("Diytail").value)
    if(valueDiytail == 99){
        valueDiytail = 99;
    } else {
        valueDiytail = valueDiytail + 1;
    }
    document.getElementById("Diytail").setAttribute("value", valueDiytail);
}

// product Diytail toevoegen aan winkelwagen
function addProductDiytail() {
    localStorage.setItem("valueDiytail", valueDiytail);
    shopingCart()
}


function shopingCart(){
    let shopingCartValueD = valueDiytail
    let shopingCartValue = localStorage.getItem("valueDiytail")
    shopingCartValue = parseInt(shopingCartValueD)

    localStorage.setItem("shopingCartValue", shopingCartValue);

    document.getElementById("quantity").setAttribute("value", shopingCartValue);
}