gsap.from(".header", {duration: 0.5, delay: 0, y: -100});
gsap.from(".text", {rotation: -360, x: -100, duration: 1});

let valueIpanema = 0
let valueLazyRedCheeksMocktail = 0
let valueTannin = 0
let valueBalicha = 0
let valueAmbrosia = 0

// af- en optelfunctie Ipanema
function aftellenIpanema() {
    valueIpanema = parseInt(document.getElementById("Ipanema").value)
    if(valueIpanema == 0){
        valueIpanema = 0;
    } else {
        valueIpanema = valueIpanema - 1;
    }
    document.getElementById("Ipanema").setAttribute("value", valueIpanema);
}

function optellenIpanema() {
    valueIpanema = parseInt(document.getElementById("Ipanema").value)
    if(valueIpanema == 99){
        valueIpanema = 99;
    } else {
        valueIpanema = valueIpanema + 1;
    }
    document.getElementById("Ipanema").setAttribute("value", valueIpanema);
}

// af- en optelfunctie Lazy Red Cheeks Mocktail
function aftellenLazyRedCheeksMocktail() {
    valueLazyRedCheeksMocktail = parseInt(document.getElementById("LazyRedCheeksMocktail").value)
    if(valueLazyRedCheeksMocktail == 0){
        valueLazyRedCheeksMocktail = 0;
    } else {
        valueLazyRedCheeksMocktail = valueLazyRedCheeksMocktail - 1;
    }
    document.getElementById("LazyRedCheeksMocktail").setAttribute("value", valueLazyRedCheeksMocktail);
}

function optellenLazyRedCheeksMocktail() {
    valueLazyRedCheeksMocktail = parseInt(document.getElementById("LazyRedCheeksMocktail").value)
    if(valueLazyRedCheeksMocktail == 99){
        valueLazyRedCheeksMocktail = 99;
    } else {
        valueLazyRedCheeksMocktail = valueLazyRedCheeksMocktail + 1;
    }
    document.getElementById("LazyRedCheeksMocktail").setAttribute("value", valueLazyRedCheeksMocktail);
}

// af- en optelfunctie Tannin
function aftellenTannin() {
    valueTannin = parseInt(document.getElementById("quantityTannin").value)
    if(valueTannin == 0){
        valueTannin = 0;
    } else {
        valueTannin = valueTannin - 1;
    }
    document.getElementById("quantityTannin").setAttribute("value", valueTannin);
}

function optellenTannin() {
    valueTannin = parseInt(document.getElementById("quantityTannin").value)
    if(valueTannin == 99){
        valueTannin = 99;
    } else {
        valueTannin = valueTannin + 1;
    }
    document.getElementById("quantityTannin").setAttribute("value", valueTannin);
}

// af- en optelfunctie Balicha
function aftellenBalicha() {
    valueBalicha = parseInt(document.getElementById("quantityBalicha").value)
    if(valueBalicha == 0){
        valueBalicha = 0;
    } else {
        valueBalicha = valueBalicha - 1;
    }
    document.getElementById("quantityBalicha").setAttribute("value", valueBalicha);
}

function optellenBalicha() {
    valueBalicha = parseInt(document.getElementById("quantityBalicha").value)
    if(valueBalicha == 99){
        valueBalicha = 99;
    } else {
        valueBalicha = valueBalicha + 1;
    }
    document.getElementById("quantityBalicha").setAttribute("value", valueBalicha);
}

// af- en optelfunctie Ambrosia
function aftellenAmbrosia() {
    valueAmbrosia = parseInt(document.getElementById("quantityAmbrosia").value)
    if(valueAmbrosia == 0){
        valueAmbrosia = 0;
    } else {
        valueAmbrosia = valueAmbrosia - 1;
    }
    document.getElementById("quantityAmbrosia").setAttribute("value", valueAmbrosia);
}

function optellenAmbrosia() {
    valueAmbrosia = parseInt(document.getElementById("quantityAmbrosia").value)
    if(valueAmbrosia == 99){
        valueAmbrosia = 99;
    } else {
        valueAmbrosia = valueAmbrosia + 1;
    }
    document.getElementById("quantityAmbrosia").setAttribute("value", valueAmbrosia);
}


// product Ipanema toevoegen aan winkelwagen
function addProductIpanema() {
    localStorage.setItem("valueIpanema", valueIpanema);
    shopingCart()
}

// product Lazy Red Cheeks Mocktail toevoegen aan winkelwagen
function addProductLazyRedCheeksMocktail() {
    localStorage.setItem("valueLazyRedCheeksMocktail", valueLazyRedCheeksMocktail);
    shopingCart()
}

// product  Tannin toevoegen aan winkelwagen
function addProductTannin() {
    localStorage.setItem("valueTannin", valueTannin);
    shopingCart()
}

// product Balicha toevoegen aan winkelwagen
function addProductBalicha() {
    localStorage.setItem("valueBalicha", valueBalicha);
    shopingCart()
}

// product Ambrosia toevoegen aan winkelwagen
function addProductAmbrosia() {
    localStorage.setItem("valueAmbrosia", valueAmbrosia);
    shopingCart()
}


function shopingCart(){    
    let shopingCartValueM = valueIpanema + valueLazyRedCheeksMocktail + valueTannin + valueBalicha + valueAmbrosia
    let shopingCartValue = localStorage.getItem("shopingCartValue")
    shopingCartValue = parseInt(shopingCartValueM)

    localStorage.setItem("shopingCartValue", shopingCartValue);

    document.getElementById("quantity").setAttribute("value", shopingCartValueM);
}