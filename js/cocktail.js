gsap.from(".header", {duration: 0.5, delay: 0, y: -100});
gsap.from(".text", {rotation: -360, x: -100, duration: 1});

let valueMojito = 0
let valueDarkStormy = 0
let valuePornstarMartini = 0
let valueLazyRedCheeks = 0
let valueSidney = 0
let valueStrawberryMojito = 0
let valueMonkeyBusiness = 0
let valueGuavaMargarita = 0

// af- en optelfunctie Mojito
function aftellenMojito() {
    valueMojito = parseInt(document.getElementById("mojito").value)
    if(valueMojito == 0){
        valueMojito = 0;
    } else {
        valueMojito = valueMojito - 1;
    }
    document.getElementById("mojito").setAttribute("value", valueMojito);
}

function optellenMojito() {
    valueMojito = parseInt(document.getElementById("mojito").value)
    if(valueMojito == 99){
        valueMojito = 99;
    } else {
        valueMojito = valueMojito + 1;
    }
    document.getElementById("mojito").setAttribute("value", valueMojito);
}

// af- en optelfunctie Dark & Stormy
function aftellenDarkStormy() {
    valueDarkStormy = parseInt(document.getElementById("darkStormy").value)
    if(valueDarkStormy == 0){
        valueDarkStormy = 0;
    } else {
        valueDarkStormy = valueDarkStormy - 1;
    }
    document.getElementById("darkStormy").setAttribute("value", valueDarkStormy);
}

function optellenDarkStormy() {
    valueDarkStormy = parseInt(document.getElementById("darkStormy").value)
    if(valueDarkStormy == 99){
        valueDarkStormy = 99;
    } else {
        valueDarkStormy = valueDarkStormy + 1;
    }
    document.getElementById("darkStormy").setAttribute("value", valueDarkStormy);
}

// af- en optelfunctie Martini
function aftellenPornstarMartini() {
    valuePornstarMartini = parseInt(document.getElementById("quantityPornstarMartini").value)
    if(valuePornstarMartini == 0){
        valuePornstarMartini = 0;
    } else {
        valuePornstarMartini = valuePornstarMartini - 1;
    }
    document.getElementById("quantityPornstarMartini").setAttribute("value", valuePornstarMartini);
}

function optellenPornstarMartini() {
    valuePornstarMartini = parseInt(document.getElementById("quantityPornstarMartini").value)
    if(valuePornstarMartini == 99){
        valuePornstarMartini = 99;
    } else {
        valuePornstarMartini = valuePornstarMartini + 1;
    }
    document.getElementById("quantityPornstarMartini").setAttribute("value", valuePornstarMartini);
}

// af- en optelfunctie Lazy Red Cheeks
function aftellenLazyRedCheeks() {
    valueLazyRedCheeks = parseInt(document.getElementById("quantityLazyRedCheeks").value)
    if(valueLazyRedCheeks == 0){
        valueLazyRedCheeks = 0;
    } else {
        valueLazyRedCheeks = valueLazyRedCheeks - 1;
    }
    document.getElementById("quantityLazyRedCheeks").setAttribute("value", valueLazyRedCheeks);
}

function optellenLazyRedCheeks() {
    valueLazyRedCheeks = parseInt(document.getElementById("quantityLazyRedCheeks").value)
    if(valueLazyRedCheeks == 99){
        valueLazyRedCheeks = 99;
    } else {
        valueLazyRedCheeks = valueLazyRedCheeks + 1;
    }
    document.getElementById("quantityLazyRedCheeks").setAttribute("value", valueLazyRedCheeks);
}

// af- en optelfunctie Sidney
function aftellenSidney() {
    valueSidney = parseInt(document.getElementById("quantitySidney").value)
    if(valueSidney == 0){
        valueSidney = 0;
    } else {
        valueSidney = valueSidney - 1;
    }
    document.getElementById("quantitySidney").setAttribute("value", valueSidney);
}

function optellenSidney() {
    valueSidney = parseInt(document.getElementById("quantitySidney").value)
    if(valueSidney == 99){
        valueSidney = 99;
    } else {
        valueSidney = valueSidney + 1;
    }
    document.getElementById("quantitySidney").setAttribute("value", valueSidney);
}

// af- en optelfunctie Strawberry Mojito
function aftellenStrawberryMojito() {
    valueStrawberryMojito = parseInt(document.getElementById("quantityStrawberryMojito").value)
    if(valueStrawberryMojito == 0){
        valueStrawberryMojito = 0;
    } else {
        valueStrawberryMojito = valueStrawberryMojito - 1;
    }
    document.getElementById("quantityStrawberryMojito").setAttribute("value", valueStrawberryMojito);
}

function optellenStrawberryMojito() {
    valueStrawberryMojito = parseInt(document.getElementById("quantityStrawberryMojito").value)
    if(valueStrawberryMojito == 99){
        valueStrawberryMojito = 99;
    } else {
        valueStrawberryMojito = valueStrawberryMojito + 1;
    }
    document.getElementById("quantityStrawberryMojito").setAttribute("value", valueStrawberryMojito);
}

// af- en optelfunctie Monkey Business
function aftellenMonkeyBusiness() {
    valueMonkeyBusiness = parseInt(document.getElementById("quantityMonkeyBusiness").value)
    if(valueMonkeyBusiness == 0){
        valueMonkeyBusiness = 0;
    } else {
        valueMonkeyBusiness = valueMonkeyBusiness - 1;
    }
    document.getElementById("quantityMonkeyBusiness").setAttribute("value", valueMonkeyBusiness);
}

function optellenMonkeyBusiness() {
    valueMonkeyBusiness = parseInt(document.getElementById("quantityMonkeyBusiness").value)
    if(valueMonkeyBusiness == 99){
        valueMonkeyBusiness = 99;
    } else {
        valueMonkeyBusiness = valueMonkeyBusiness + 1;
    }
    document.getElementById("quantityMonkeyBusiness").setAttribute("value", valueMonkeyBusiness);
}

// af- en optelfunctie Guava Margarita
function aftellenGuavaMargarita() {
    valueGuavaMargarita = parseInt(document.getElementById("quantityGuavaMargarita").value)
    if(valueGuavaMargarita == 0){
        valueGuavaMargarita = 0;
    } else {
        valueGuavaMargarita = valueGuavaMargarita - 1;
    }
    document.getElementById("quantityGuavaMargarita").setAttribute("value", valueGuavaMargarita);
}

function optellenGuavaMargarita() {
    valueGuavaMargarita = parseInt(document.getElementById("quantityGuavaMargarita").value)
    if(valueGuavaMargarita == 99){
        valueGuavaMargarita = 99;
    } else {
        valueGuavaMargarita = valueGuavaMargarita + 1;
    }
    document.getElementById("quantityGuavaMargarita").setAttribute("value", valueGuavaMargarita);
}


// product Mojito toevoegen aan winkelwagen
function addProductMojito() {
    localStorage.setItem("valueMojito", valueMojito);
    shopingCart()
}

// product Dark Stormy toevoegen aan winkelwagen
function addProductDarkStormy() {
    localStorage.setItem("valueDarkStormy", valueDarkStormy);
    shopingCart()
}

// product Pornstar Martini toevoegen aan winkelwagen
function addProductPornstarMartini() {
    localStorage.setItem("valuePornstarMartini", valuePornstarMartini);
    shopingCart()
}

// product Lazy Red Cheeks toevoegen aan winkelwagen
function addProductLazyRedCheeks() {
    localStorage.setItem("valueLazyRedCheeks", valueLazyRedCheeks);
    shopingCart()
}

// product Sidney toevoegen aan winkelwagen
function addProductSidney() {
    localStorage.setItem("valueSidney", valueSidney);
    shopingCart()
}

// product Strawberry Mojito toevoegen aan winkelwagen
function addProductStrawberryMojito() {
    localStorage.setItem("valueStrawberryMojito", valueStrawberryMojito);
    shopingCart()
}

// product Monkey Business toevoegen aan winkelwagen
function addProductMonkeyBusiness() {
    localStorage.setItem("valueMonkeyBusiness", valueMonkeyBusiness);
    shopingCart()
}

// product Guava Margarita toevoegen aan winkelwagen
function addProductGuavaMargarita() {
    localStorage.setItem("valueGuavaMargarita", valueGuavaMargarita);
    shopingCart()
}


function shopingCart(){    
    let shopingCartValueK = valueMojito + valueDarkStormy + valuePornstarMartini + valueLazyRedCheeks + valueSidney + valueStrawberryMojito + valueMonkeyBusiness + valueGuavaMargarita
    var shopingCartValue = localStorage.getItem("shopingCartValue")
    shopingCartValue = parseInt(shopingCartValueK)

    localStorage.setItem("shopingCartValue", shopingCartValue);

    document.getElementById("quantity").setAttribute("value", shopingCartValue);
}

// function shopingCart(){
//     let shopingCartValueK = valueMojito + valueDarkStormy + valuePornstarMartini + valueLazyRedCheeks + valueSidney + valueStrawberryMojito + valueMonkeyBusiness + valueGuavaMargarita
//     localStorage.setItem("shopingCartValue", shopingCartValueK);
//     document.getElementById("quantity").setAttribute("value", shopingCartValueK);
// }

