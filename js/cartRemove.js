// REMOVE------------------------------------------------------------------------------------------------------------
// COCKTAILS------------------------------------------------------------------------------------------------------------
function removeMojito() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueMojito = parseInt(document.getElementById("valueMojito").value) 
    let prijsMojitoTemp = 14 * valueMojito
    prijsTotaal = prijsTotaal - prijsMojitoTemp 

    var divMojito = document.getElementById("cartMojito");
    divMojito.style.display = 'hidden';

    valueMojito = 0;
    document.getElementById("valueMojito").innerHTML = valueMojito;
    localStorage.setItem("valueMojito", valueMojito);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeDarkStormy() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)  
    let valueDarkStormy = parseInt(document.getElementById("valueDarkStormy").value) 
    let prijsDarkStormyTemp = 14 * valueDarkStormy
    prijsTotaal = prijsTotaal - prijsDarkStormyTemp 

    var divDarkStormy = document.getElementById("cartDarkStormy");
    divDarkStormy.style.display = 'hidden';
    
    valueDarkStormy = 0;
    document.getElementById("valueDarkStormy").innerHTML = valueDarkStormy;
    localStorage.setItem("valueDarkStormy", valueDarkStormy);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removePornstarMartini() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valuePornstarMartini = parseInt(document.getElementById("valuePornstarMartini").value) 
    let prijsPornstarMartiniTemp = 14 * valuePornstarMartini
    prijsTotaal = prijsTotaal - prijsPornstarMartiniTemp 

    var divPornstarMartini = document.getElementById("cartPornstarMartini");
    divPornstarMartini.style.display = 'hidden';

    valuePornstarMartini = 0;
    document.getElementById("valuePornstarMartini").innerHTML = valuePornstarMartini;
    localStorage.setItem("valuePornstarMartini", valuePornstarMartini);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeLazyRedCheeks() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueLazyRedCheeks = parseInt(document.getElementById("valueLazyRedCheeks").value) 
    let prijsLazyRedCheeksTemp = 14 * valueLazyRedCheeks
    prijsTotaal = prijsTotaal - prijsLazyRedCheeksTemp 

    var divLazyRedCheeks = document.getElementById("cartLazyRedCheeks");
    divLazyRedCheeks.style.display = 'hidden';

    valueLazyRedCheeks = 0;
    document.getElementById("valueLazyRedCheeks").innerHTML = valueLazyRedCheeks;
    localStorage.setItem("valueLazyRedCheeks", valueLazyRedCheeks);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeSidney() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueSidney = parseInt(document.getElementById("valueSidney").value) 
    let prijsSidneyTemp = 14 * valueSidney
    prijsTotaal = prijsTotaal - prijsSidneyTemp 

    var divSidney = document.getElementById("cartSidney");
    divSidney.style.display = 'hidden';

    valueSidney = 0;
    document.getElementById("valueSidney").innerHTML = valueSidney;
    localStorage.setItem("valueSidney", valueSidney);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeStrawberryMojito() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueStrawberryMojito = parseInt(document.getElementById("valueStrawberryMojito").value) 
    let prijsStrawberryMojitoTemp = 14 * valueStrawberryMojito
    prijsTotaal = prijsTotaal - prijsStrawberryMojitoTemp 

    var divStrawberryMojito = document.getElementById("cartStrawberryMojito");
    divStrawberryMojito.style.display = 'hidden';

    valueStrawberryMojito = 0;
    document.getElementById("valueStrawberryMojito").innerHTML = valueStrawberryMojito;
    localStorage.setItem("valueStrawberryMojito", valueStrawberryMojito);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeMonkeyBusiness() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueMonkeyBusiness = parseInt(document.getElementById("valueMonkeyBusiness").value) 
    let prijsMonkeyBusinessTemp = 14 * valueMonkeyBusiness
    prijsTotaal = prijsTotaal - prijsMonkeyBusinessTemp 

    var divMonkeyBusiness = document.getElementById("cartMonkeyBusiness");
    divMonkeyBusiness.style.display = 'hidden';

    valueMonkeyBusiness = 0;
    document.getElementById("valueMonkeyBusiness").innerHTML = valueMonkeyBusiness;
    localStorage.setItem("valueMonkeyBusiness", valueMonkeyBusiness);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeGuavaMargarita() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueGuavaMargarita = parseInt(document.getElementById("valueGuavaMargarita").value) 
    let prijsGuavaMargaritaTemp = 14 * valueGuavaMargarita
    prijsTotaal = prijsTotaal - prijsGuavaMargaritaTemp 

    var divGuavaMargarita = document.getElementById("cartGuavaMargarita");
    divGuavaMargarita.style.display = 'hidden';

    valueGuavaMargarita = 0;
    document.getElementById("valueGuavaMargarita").innerHTML = valueGuavaMargarita;
    localStorage.setItem("valueGuavaMargarita", valueGuavaMargarita);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

// MOCKTAILS------------------------------------------------------------------------------------------------------------
function removeIpanema() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueIpanema = parseInt(document.getElementById("valueIpanema").value) 
    let prijsIpanemaTemp = 14 * valueIpanema
    prijsTotaal = prijsTotaal - prijsIpanemaTemp 

    var divIpanema = document.getElementById("cartIpanema");
    divIpanema.style.display = 'hidden';

    valueIpanema = 0;
    document.getElementById("valueIpanema").innerHTML = valueIpanema;
    localStorage.setItem("valueIpanema", valueIpanema);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeLazyRedCheeksMocktail() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueLazyRedCheeksMocktail = parseInt(document.getElementById("valueLazyRedCheeksMocktail").value) 
    let prijsLazyRedCheeksMocktailTemp = 14 * valueLazyRedCheeksMocktail
    prijsTotaal = prijsTotaal - prijsLazyRedCheeksMocktailTemp 

    var divLazyRedCheeksMocktail = document.getElementById("cartLazyRedCheeksMocktail");
    divLazyRedCheeksMocktail.style.display = 'hidden';

    valueLazyRedCheeksMocktail = 0;
    document.getElementById("valueLazyRedCheeksMocktail").innerHTML = valueLazyRedCheeksMocktail;
    localStorage.setItem("valueLazyRedCheeksMocktail", valueLazyRedCheeksMocktail);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeTannin() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueTannin = parseInt(document.getElementById("valueTannin").value) 
    let prijsTanninTemp = 14 * valueTannin
    prijsTotaal = prijsTotaal - prijsTanninTemp 

    var divTannin = document.getElementById("cartTannin");
    divTannin.style.display = 'hidden';

    valueTannin = 0;
    document.getElementById("valueTannin").innerHTML = valueTannin;
    localStorage.setItem("valueTannin", valueTannin);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeBalicha() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueBalicha = parseInt(document.getElementById("valueBalicha").value) 
    let prijsBalichaTemp = 14 * valueBalicha
    prijsTotaal = prijsTotaal - prijsBalichaTemp 

    var divBalicha = document.getElementById("cartBalicha");
    divBalicha.style.display = 'hidden';

    valueBalicha = 0;
    document.getElementById("valueBalicha").innerHTML = valueBalicha;
    localStorage.setItem("valueBalicha", valueBalicha);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function removeAmbrosia() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueAmbrosia = parseInt(document.getElementById("valueAmbrosia").value) 
    let prijsAmbrosiaTemp = 14 * valueAmbrosia
    prijsTotaal = prijsTotaal - prijsAmbrosiaTemp 

    var divAmbrosia = document.getElementById("cartAmbrosia");
    divAmbrosia.style.display = 'hidden';

    valueAmbrosia = 0;
    document.getElementById("valueAmbrosia").innerHTML = valueAmbrosia;
    localStorage.setItem("valueAmbrosia", valueAmbrosia);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

// DIYTAILS------------------------------------------------------------------------------------------------------------
function removeDiytail() {
    let prijsTotaal = parseInt(document.getElementById("totaal").value)
    let valueDiytail = parseInt(document.getElementById("valueDiytail").value) 
    let prijsDiytailTemp = 14 * valueDiytail
    prijsTotaal = prijsTotaal - prijsDiytailTemp 

    var divDiytail = document.getElementById("cartDiytail");
    divDiytail.style.display = 'hidden';

    valueDiytail = 0;
    document.getElementById("valueDiytail").innerHTML = valueDiytail;
    localStorage.setItem("valueDiytail", valueDiytail);

    document.getElementById("totaal").innerHTML = prijsTotaal;

    cartEmpty()

    setTimeout(redirectPage, 10);
    function redirectPage() {
        window.location.href = "cart.html";
    }
}

function cartEmpty() {
    localStorage.setItem("shopingCartValue", 0);
}