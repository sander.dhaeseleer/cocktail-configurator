gsap.from(".header", {duration: 0.5, delay: 0, y: -100});
gsap.from(".text", {rotation: -360, x: -100, duration: 1});

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, set, ref, child, get } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyAPydTC3t9pwE1xOEfLorA4VGX6MeQjhhs",
    authDomain: "cocktail-configurator-17d09.firebaseapp.com",
    projectId: "cocktail-configurator-17d09",
    storageBucket: "cocktail-configurator-17d09.appspot.com",
    messagingSenderId: "1048309800453",
    appId: "1:1048309800453:web:43ba73eccc7778ff9dc66c"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);

window.insertData = function () {
    if (prijsTotaal > 0) {
        var name = prompt("Gelieve uw naam op te geven:", "");
        if (name == null || name == "") {
            alert("Bestelling geannuleerd");
        } 
        else {

            // COCKTAILS------------------------------------------------------------------------------------------------------------
            if (valueMojito > 0) {
                let prijsTotaalCocktail = prijsMojito * valueMojito
                set(ref(database, "Orders/Mojito/" + name), {
                    Naam: name,
                    CocktailAantal: valueMojito,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueDarkStormy > 0) {
                let prijsTotaalCocktail = prijsDarkStormy * valueDarkStormy
                set(ref(database, "Orders/DarkStormy/" + name), {
                    Naam: name,
                    CocktailAantal: valueDarkStormy,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valuePornstarMartini > 0) {
                let prijsTotaalCocktail = prijsPornstarMartini * valuePornstarMartini
                set(ref(database, "Orders/Pornstar Martini/" + name), {
                    Naam: name,
                    CocktailAantal: valuePornstarMartini,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueLazyRedCheeks > 0) {
                let prijsTotaalCocktail = prijsLazyRedCheeks * valueLazyRedCheeks
                set(ref(database, "Orders/Lazy Red Cheeks/" + name), {
                    Naam: name,
                    CocktailAantal: valueLazyRedCheeks,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueSidney > 0) {
                let prijsTotaalCocktail = prijsSidney * valueSidney
                set(ref(database, "Orders/Sidney/" + name), {
                    Naam: name,
                    CocktailAantal: valueSidney,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueStrawberryMojito > 0) {
                let prijsTotaalCocktail = prijsStrawberryMojito * valueStrawberryMojito
                set(ref(database, "Orders/Strawberry Mojito/" + name), {
                    Naam: name,
                    CocktailAantal: valueStrawberryMojito,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueMonkeyBusiness > 0) {
                let prijsTotaalCocktail = prijsMonkeyBusiness * valueMonkeyBusiness
                set(ref(database, "Orders/Monkey Business/" + name), {
                    Naam: name,
                    CocktailAantal: valueMonkeyBusiness,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueGuavaMargarita > 0) {
                let prijsTotaalCocktail = prijsGuavaMargarita * valueGuavaMargarita
                set(ref(database, "Orders/Guava Margarita/" + name), {
                    Naam: name,
                    CocktailAantal: valueGuavaMargarita,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            // MOCKTAILS------------------------------------------------------------------------------------------------------------
            if (valueIpanema > 0) {
                let prijsTotaalCocktail = prijsIpanema * valueIpanema
                set(ref(database, "Orders/Ipanema/" + name), {
                    Naam: name,
                    CocktailAantal: valueIpanema,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueLazyRedCheeksMocktail > 0) {
                let prijsTotaalCocktail = prijsLazyRedCheeksMocktail * valueLazyRedCheeksMocktail
                set(ref(database, "Orders/LazyRedCheeksMocktail/" + name), {
                    Naam: name,
                    CocktailAantal: valueLazyRedCheeksMocktail,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueTannin > 0) {
                let prijsTotaalCocktail = prijsTannin * valueTannin
                set(ref(database, "Orders/Tannin/" + name), {
                    Naam: name,
                    CocktailAantal: valueTannin,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueBalicha > 0) {
                let prijsTotaalCocktail = prijsBalicha * valueBalicha
                set(ref(database, "Orders/Balicha/" + name), {
                    Naam: name,
                    CocktailAantal: valueBalicha,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            if (valueAmbrosia > 0) {
                let prijsTotaalCocktail = prijsAmbrosia * valueAmbrosia
                set(ref(database, "Orders/Ambrosia/" + name), {
                    Naam: name,
                    CocktailAantal: valueAmbrosia,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            // DIYTAILS------------------------------------------------------------------------------------------------------------
            if (valueDiytail > 0) {
                let prijsTotaalCocktail = prijsDiytail * valueDiytail
                set(ref(database, "Orders/Diytail/" + name), {
                    Naam: name,
                    CocktailAantal: valueDiytail,
                    PrijsTotaal: prijsTotaalCocktail
                })
                .then(() => {
                    alert("Uw bestelling is succesvol opgenomen!");
                })
            }

            localStorage.setItem("valueMojito", "0");
            localStorage.setItem("valueDarkStormy", "0");
            localStorage.setItem("valuePornstarMartini", "0");
            localStorage.setItem("valueLazyRedCheeks", "0");
            localStorage.setItem("valueSidney", "0");
            localStorage.setItem("valueStrawberryMojito", "0");
            localStorage.setItem("valueMonkeyBusiness", "0");
            localStorage.setItem("valueGuavaMargarita", "0");

            localStorage.setItem("valueIpanema", "0");
            localStorage.setItem("valueLazyRedCheeksMocktail", "0");
            localStorage.setItem("valueTannin", "0");
            localStorage.setItem("valueBalicha", "0");
            localStorage.setItem("valueAmbrosia", "0");

            localStorage.setItem("valueDiytail", "0");

            localStorage.setItem("shopingCartValue", "0");

            // localStorage.setItem("valueCart", "0");

            setTimeout(redirectPage, 3000);
            function redirectPage() {
                window.location.href = "index.html";
            }
        }
    }
}

var valueMojito = localStorage.getItem("valueMojito")
var valueDarkStormy = localStorage.getItem("valueDarkStormy")
var valuePornstarMartini = localStorage.getItem("valuePornstarMartini")
var valueLazyRedCheeks = localStorage.getItem("valueLazyRedCheeks")
var valueSidney = localStorage.getItem("valueSidney")
var valueStrawberryMojito = localStorage.getItem("valueStrawberryMojito")
var valueMonkeyBusiness = localStorage.getItem("valueMonkeyBusiness")
var valueGuavaMargarita = localStorage.getItem("valueGuavaMargarita")

var valueIpanema = localStorage.getItem("valueIpanema")
var valueLazyRedCheeksMocktail = localStorage.getItem("valueLazyRedCheeksMocktail")
var valueTannin = localStorage.getItem("valueTannin")
var valueBalicha = localStorage.getItem("valueBalicha")
var valueAmbrosia = localStorage.getItem("valueAmbrosia")

var valueDiytail = localStorage.getItem("valueDiytail")

let prijsTotaal = 0;

let prijsMojito = 14;
let prijsDarkStormy = 14;
let prijsPornstarMartini = 14;
let prijsLazyRedCheeks = 14;
let prijsSidney = 14;
let prijsStrawberryMojito = 14;
let prijsMonkeyBusiness = 14;
let prijsGuavaMargarita = 14;

let prijsIpanema = 14;
let prijsLazyRedCheeksMocktail = 14;
let prijsTannin = 14;
let prijsBalicha= 14;
let prijsAmbrosia = 14;

let prijsDiytail = 15;


// COCKTAILS------------------------------------------------------------------------------------------------------------
if (valueMojito > 0) {
    var divMojito = document.getElementById("cartMojito");
    divMojito.style.display = 'block';
    document.getElementById("valueMojito").innerHTML = valueMojito;
    let prijsMojitoTemp = prijsMojito * valueMojito
    prijsTotaal = prijsTotaal + prijsMojitoTemp
}

if (valueDarkStormy > 0) {
    var divDarkStormy = document.getElementById("cartDarkStormy");
    divDarkStormy.style.display = 'block';
    document.getElementById("valueDarkStormy").innerHTML = valueDarkStormy;
    let prijsDarkStormyTemp = prijsDarkStormy * valueDarkStormy
    prijsTotaal = prijsTotaal + prijsDarkStormyTemp
}

if (valuePornstarMartini > 0) {
    var divPornstarMartini = document.getElementById("cartPornstarMartini");
    divPornstarMartini.style.display = 'block';
    document.getElementById("valuePornstarMartini").innerHTML = valuePornstarMartini;
    let prijsPornstarMartiniTemp = prijsPornstarMartini * valuePornstarMartini
    prijsTotaal = prijsTotaal + prijsPornstarMartiniTemp
}

if (valueLazyRedCheeks > 0) {
    var divLazyRedCheeks = document.getElementById("cartLazyRedCheeks");
    divLazyRedCheeks.style.display = 'block';
    document.getElementById("valueLazyRedCheeks").innerHTML = valueLazyRedCheeks;
    let prijsLazyRedCheeksTemp = prijsLazyRedCheeks * valueLazyRedCheeks
    prijsTotaal = prijsTotaal + prijsLazyRedCheeksTemp
}

if (valueSidney > 0) {
    var divSidney = document.getElementById("cartSidney");
    divSidney.style.display = 'block';
    document.getElementById("valueSidney").innerHTML = valueSidney;
    let prijsSidneyTemp = prijsSidney * valueSidney
    prijsTotaal = prijsTotaal + prijsSidneyTemp
}

if (valueStrawberryMojito > 0) {
    var divStrawberryMojito = document.getElementById("cartStrawberryMojito");
    divStrawberryMojito.style.display = 'block';
    document.getElementById("valueStrawberryMojito").innerHTML = valueStrawberryMojito;
    let prijsStrawberryMojitoTemp = prijsStrawberryMojito * valueStrawberryMojito
    prijsTotaal = prijsTotaal + prijsStrawberryMojitoTemp
}

if (valueMonkeyBusiness > 0) {
    var divMonkeyBusiness = document.getElementById("cartMonkeyBusiness");
    divMonkeyBusiness.style.display = 'block';
    document.getElementById("valueMonkeyBusiness").innerHTML = valueMonkeyBusiness;
    let prijsMonkeyBusinessTemp = prijsMonkeyBusiness * valueMonkeyBusiness
    prijsTotaal = prijsTotaal + prijsMonkeyBusinessTemp
}

if (valueGuavaMargarita > 0) {
    var divGuavaMargarita = document.getElementById("cartGuavaMargarita");
    divGuavaMargarita.style.display = 'block';
    document.getElementById("valueGuavaMargarita").innerHTML = valueGuavaMargarita;
    let prijsGuavaMargaritaTemp = prijsGuavaMargarita * valueGuavaMargarita
    prijsTotaal = prijsTotaal + prijsGuavaMargaritaTemp
}

// MOCKTAILS------------------------------------------------------------------------------------------------------------
if (valueIpanema > 0) {
    var divIpanema = document.getElementById("cartIpanema");
    divIpanema.style.display = 'block';
    document.getElementById("valueIpanema").innerHTML = valueIpanema;
    let prijsIpanemaTemp = prijsIpanema * valueIpanema
    prijsTotaal = prijsTotaal + prijsIpanemaTemp
}

if (valueLazyRedCheeksMocktail > 0) {
    var divLazyRedCheeksMocktail = document.getElementById("cartLazyRedCheeksMocktail");
    divLazyRedCheeksMocktail.style.display = 'block';
    document.getElementById("valueLazyRedCheeksMocktail").innerHTML = valueLazyRedCheeksMocktail;
    let prijsLazyRedCheeksMocktailTemp = prijsLazyRedCheeksMocktail * valueLazyRedCheeksMocktail
    prijsTotaal = prijsTotaal + prijsLazyRedCheeksMocktailTemp
}

if (valueTannin > 0) {
    var divTannin = document.getElementById("cartTannin");
    divTannin.style.display = 'block';
    document.getElementById("valueTannin").innerHTML = valueTannin;
    let prijsTanninTemp = prijsTannin * valueTannin
    prijsTotaal = prijsTotaal + prijsTanninTemp
}

if (valueBalicha > 0) {
    var divBalicha = document.getElementById("cartBalicha");
    divBalicha.style.display = 'block';
    document.getElementById("valueBalicha").innerHTML = valueBalicha;
    let prijsBalichaTemp = prijsBalicha * valueBalicha
    prijsTotaal = prijsTotaal + prijsBalichaTemp
}

if (valueAmbrosia > 0) {
    var divAmbrosia = document.getElementById("cartAmbrosia");
    divAmbrosia.style.display = 'block';
    document.getElementById("valueAmbrosia").innerHTML = valueAmbrosia;
    let prijsAmbrosiaTemp = prijsAmbrosia * valueAmbrosia
    prijsTotaal = prijsTotaal + prijsAmbrosiaTemp
}

// DIYTAILS------------------------------------------------------------------------------------------------------------
if (valueDiytail > 0) {
    var divDiytail = document.getElementById("cartDiytail");
    divDiytail.style.display = 'block';
    document.getElementById("valueDiytail").innerHTML = valueDiytail;
    let prijsDiytailTemp = prijsDiytail * valueDiytail
    prijsTotaal = prijsTotaal + prijsDiytailTemp
}

document.getElementById("totaal").innerHTML = prijsTotaal;